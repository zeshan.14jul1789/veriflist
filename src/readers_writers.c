/**Readers_writers.c
 * 
 * 
 * This file will house an implimentation of readers and
 * writers. The writers will write an integer to stdout, 
 * while the readers will print it out. 
 * The number of readers and writers will be random 
 * from the start. 
/** List.h 
 * 
 *  This file will house all of the function declarations 
 *  that will be used by the list library. It will also 
 *  contain the structure definitions that will be used, 
 *  namely node and list. 
 * */

typedef struct node{
    struct node* prev;
    struct node* next;
} node;

typedef struct list {
    node *head; 
    node *tail;    
    int  size;
} list;

// LIST ALLOCATION #####################################
/*create a list*/
list *list_create(); //essential

/*list delete*/
int list_delete(list *aList); //essential


//LIST ADDERS ##########################################
/*add first*/
int list_add_first(list *aList, void *item); //essential
/*add last*/
int list_add_last(list *aList, void *item); //essential

/*add nth*/ //extra


//LIST REMOVERS ########################################
/*remove first*/
void *list_remove_first(list* aList); //essential
/*remove last*/
void *list_remove_last(list *aList); //essential

/*remove nth?*/

//LIST GETTERS #########################################
/*list length*/
int list_length(list *aList); //essential
/*free list count*/
int list_available_count(); //essential
/*free node count*/

int node_available_count(); //essential

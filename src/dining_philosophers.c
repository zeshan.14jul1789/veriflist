//Dining Philosophers
//A simple implimentation of dining philosophers
#include<stdlib.h>
#include<unistd.h>
#include<pthread.h>
#include<stdio.h>

int forks[] = {1, 1, 1, 1, 1};
pthread_mutex_t fork_mutex = PTHREAD_MUTEX_INITIALIZER; 

int philosopher_number[] = {0, 1, 2, 3, 4};

void think(int *pnumber){
    printf("Philosopher %d is thinking \n", *pnumber);

    float think_time = 2.0 * (rand() / (float)RAND_MAX);
    sleep(think_time);
}

void eat(int *pnumber){
    printf("Philosopher %d is eating \n", *pnumber);

    float eat_time = 2.0 * (rand() / (float)RAND_MAX);
    sleep(eat_time);

    pthread_mutex_lock(&fork_mutex);
    forks[*pnumber] = 1;
    forks[(*pnumber + 1) % 4] = 1;
    pthread_mutex_unlock(&fork_mutex);
    printf("Philosopher %d is done eating \n", *pnumber);
}

void grabFork(int *pnumber){
    int rightFork = *pnumber; 
    int leftFork = (*pnumber + 1) % 5;

    //try to grabfork
    pthread_mutex_lock(&fork_mutex);

    if (forks[rightFork] && forks[leftFork]){
        forks[rightFork] = 0;
        forks[leftFork] = 0;
        pthread_mutex_unlock(&fork_mutex);
        printf("Philosopher %d got forks \n", *pnumber);
        eat(pnumber);
    }else{
        pthread_mutex_unlock(&fork_mutex);
    }


}

void philosopher(int *pnumber){
    for(;;){
        think(pnumber);
        grabFork(pnumber);
    }
}


int main(int argc, char **argv){ 
    pthread_t phil[5];

    for(int i = 0; i < 5; i++){
        pthread_create(phil + i, NULL, philosopher, philosopher_number + i);
    }

    for(int i = 0; i < 5; i++)
        pthread_join(phil[i], NULL);
        
}